from components.storage.db.DbBook import DbBook as Book
from models.Link import Link

# b = Book(
#     name='Война и мир',
#     author='Толстой',
#     year='1860',
#     publish='Питер'
# )
_book = Book()
books = _book.all()
for book in books:
    print(book.id)
    print(book.name)
print(_book.one(1615456953).name)
_book.delete(1615456953)
for book in _book.all():
    print(book.id)
    print(book.name)

l = Link(
    name="Google",
    link="https://google.com"
)