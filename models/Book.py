from models.Media import Media
from models.Media import TYPE_BOOK


class Book(Media):
    name: str
    author: str
    year: int
    publish: str

    def __init__(self, **kwargs):
        """
        Создание экземпляра книги

        :param name: str
        :param author: str
        :param year: int
        :param publish: str
        """
        super().__init__(TYPE_BOOK, **kwargs)

    # def __str__(self):
    #     return f'Тип: {self.type_name()}. Название: {self.name}, Год: {self.year}'
