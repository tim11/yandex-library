TYPE_BOOK = 0
TYPE_LINK = 1
TYPE_VIDEO = 2


class Media:
    id: int
    type: int

    def __init__(self, type: int, **kwargs: dict):
        """
        Args:
            int type: Тип медиа
            dict **kwargs: Аттрибуты
        """
        self.id = None
        self.type = type
        if len(kwargs):
            for key, value in kwargs.items():
                # print(key, hasattr(self, key))
                # if hasattr(self, key):
                setattr(self, key, value)

    def type_name(self):
        if self.type == TYPE_BOOK:
            return 'Книга'
        elif self.type == TYPE_LINK:
            return 'Ссылка'
        elif self.type == TYPE_VIDEO:
            return 'Видео'

    def attributes(self):
        return self.__dict__.keys()

    def save(self):
        pass

    def all(self):
        pass

    def one(self, id):
        pass

    def delete(self, id):
        pass