from models.Media import Media
from models.Media import TYPE_LINK


class Link(Media):
    link = None
    name = None

    def __init__(self, **kwargs):
        """

        :param kwargs:
        """
        super().__init__(TYPE_LINK, **kwargs)

    def __str__(self):
        return f'Тип: {self.type_name()}. Ссылка: {self.link}'