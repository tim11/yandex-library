from models.Book import Book
import json
import time
import os
import pathlib


class FileBook(Book):

    __filename = os.path.dirname(__file__) + "/../../../data/files/books.json"
    __storage = {}

    def __init__(self, **kwargs):
        if not pathlib.Path(self.__filename).is_file():
            os.path.dirname(self.__filename)
            dirname = os.path.dirname(self.__filename)
            if not os.path.isdir(dirname):
                pathlib.Path(dirname).mkdir(
                    mode=0o775, parents=True, exist_ok=True
                )
            with open(self.__filename, "w") as write_file:
                json.dump({}, write_file)
        with open(self.__filename, "r") as read_file:
            self.__storage = json.load(read_file)
        super().__init__(**kwargs)

    def save(self):
        if not self.id:
            self.id = int(time.time())
        data = self.__storage
        data[self.id] = {
            'id': self.id,
            'name': self.name,
            'year': self.year,
            'author': self.year,
            'publish': self.publish
        }
        with open(self.__filename, "w") as write_file:
            json.dump(data, write_file)
        return self

    def all(self):
        return [
            FileBook(**val)
            for val
            in self.__storage.values()
        ]

    def one(self, id):
        value = self.__storage.get(str(id), None)
        if value:
            return FileBook(
                **value
            )
        return None

    def delete(self, id):
        if self.__storage.get(str(id), None):
            del self.__storage[str(id)]
            with open(self.__filename, "w") as write_file:
                json.dump(self.__storage, write_file)
                return True
        return False



